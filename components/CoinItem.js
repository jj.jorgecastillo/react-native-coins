import React from "react";
import { View, Text, Image } from "react-native";
import {
	containerItem,
	coinName,
	imageStyle,
	containerNames,
	text,
	symbolStyle,
	textPrice,
	pricePercentage,
	priceUp,
	priceDown,
} from "../Styles/CoinItemStyle.js";

const CoinItem = ({ coin }) => {
	const { image, name, symbol, current_price, price_change_percentage_24h } = coin;

	return (
		<View style={containerItem}>
			<View style={coinName}>
				<Image style={imageStyle} source={{ uri: image }} />
				<View style={containerNames}>
					<Text style={text}>{name}</Text>
					<Text style={symbolStyle}>{symbol}</Text>
				</View>
			</View>
			<View>
				<Text style={textPrice}>$ {current_price}</Text>
				<Text
					style={[
						pricePercentage,
						price_change_percentage_24h > 0 ? priceUp : priceDown,
					]}>
					$ {price_change_percentage_24h}
				</Text>
			</View>
		</View>
	);
};

export default CoinItem;
