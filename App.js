import React, { useEffect, useState } from "react";
import { View, FlatList, TextInput, StatusBar } from "react-native";
import { Text } from "react-native-elements";
import CoinItem from "./components/CoinItem";
import { container, header, title, searchList, list } from "./Styles/AppStyle";
import { placeholderTextColor } from "./Styles/GlobalColor";

const API_COINS =
	"https://api.coingecko.com/api/v3/coins/markets?vs_currency=usd&order=market_cap_desc&per_page=100&page=1&sparkline=false";

const App = () => {
	const [coins, setCoins] = useState([]);
	const [search, setSearch] = useState("");
	const [refreshing, setRefreshing] = useState(false);

	const loadCoinsData = async () => {
		const res = await fetch(API_COINS);
		const data = await res.json();
		setCoins(data);
	};

	useEffect(() => {
		loadCoinsData();
	}, []);

	const validator = (value) =>
		value
			? value
					.charAt(0)
					.toLowerCase()
					.concat(value.substring(1, value.length))
					.includes(search)
			: null;

	return (
		<View style={container}>
			<StatusBar backgroundColor='#141414' />
			<View style={header}>
				<Text h1 h1styles={{ title }}>
					CodeCastleCoins
				</Text>
			</View>
			<TextInput
				style={searchList}
				placeholder='Busca tu moneda'
				placeholderTextColor={placeholderTextColor}
				onChangeText={(text) => setSearch(text.toLowerCase())}
			/>
			<FlatList
				style={list}
				data={coins.filter(
					({ symbol, name }) => validator(name) || symbol.includes(search)
				)}
				renderItem={({ item }) => <CoinItem coin={item} />}
				showsHorizontalScrollIndicator={false}
				refreshing={refreshing}
				onRefresh={async () => {
					setRefreshing(true);
					await loadCoinsData();
					setRefreshing(false);
				}}
			/>
		</View>
	);
};

export default App;
