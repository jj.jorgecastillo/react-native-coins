import { StyleSheet } from "react-native";
import {
	primaryColor,
	priceUpColor,
	priceDownColor,
	symbolStyleColor,
} from "./GlobalColor";

const styles = StyleSheet.create({
	containerItem: {
		backgroundColor: "#121212",
		paddingTop: 10,
		flexDirection: "row",
		justifyContent: "space-between",
	},
	coinName: { flexDirection: "row", alignItems: "center" },
	containerNames: { marginLeft: 10 },
	imageStyle: { width: 30, height: 30 },
	text: { color: primaryColor },
	symbolStyle: { color: symbolStyleColor, textTransform: "uppercase" },
	pricePercentage: { textAlign: "right" },
	textPrice: { textAlign: "right", color: primaryColor },
	priceUp: { color: priceUpColor },
	priceDown: { color: priceDownColor },
});

export const {
	containerItem,
	coinName,
	imageStyle,
	containerNames,
	text,
	symbolStyle,
	textPrice,
	pricePercentage,
	priceUp,
	priceDown,
} = styles;
