const primaryColor = "#ffffff";
const priceUpColor = "#00B5B9";
const priceDownColor = "#FC4422";
const placeholderTextColor = "#858585";
const borderBottomColor = "#4657CE";
const symbolStyleColor = "#434343";

export {
	primaryColor,
	priceUpColor,
	priceDownColor,
	placeholderTextColor,
	borderBottomColor,
	symbolStyleColor,
};
