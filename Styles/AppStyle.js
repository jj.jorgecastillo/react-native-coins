import { StyleSheet } from "react-native";
import { primaryColor, borderBottomColor } from "./GlobalColor";

const styles = StyleSheet.create({
	container: { backgroundColor: "#141414", alignItems: "center", flex: 1 },
	header: { flexDirection: "row", justifyContent: "center", width: "100%" },
	title: { color: primaryColor, fontSize: 20 },
	list: { width: "90%" },
	searchList: {
		color: primaryColor,
		borderBottomColor,
		borderBottomWidth: 1,
		width: "50%",
		textAlign: "center",
		marginBottom: 10,
	},
});

export const { container, header, title, searchList, list } = styles;
